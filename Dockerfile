FROM gaesi/nginx-alpine-python

WORKDIR /usr/share/nginx/html

COPY ./ .

RUN pip3 install -r requirements.txt
ENTRYPOINT flask run --host=0.0.0.0
